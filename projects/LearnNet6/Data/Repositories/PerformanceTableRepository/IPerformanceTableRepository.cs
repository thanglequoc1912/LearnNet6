﻿using LearnNet6.Data.Entity;

namespace LearnNet6.Data.Repositories
{
    public interface IPerformanceTableRepository : IBaseRepository<PerformanceTable>
    {
    }
}
